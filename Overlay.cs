﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing.Drawing2D;
using System.Threading;

namespace DesktopSnow
{
    public partial class Overlay : Form
    {
        static public readonly Color TRANSPARENCY_COLOR = Color.White;
        SnowGenerator Snow;

        public Overlay()
        {
            InitializeComponent();

            this.BackColor = TRANSPARENCY_COLOR;
            this.TransparencyKey = TRANSPARENCY_COLOR;
            this.FormBorderStyle = FormBorderStyle.None;

            Rectangle ScreenRes = Screen.PrimaryScreen.WorkingArea;
            this.Size = new Size(ScreenRes.Width, ScreenRes.Height);
            this.StartPosition = FormStartPosition.Manual;
            this.Location = new Point(0, 0);
            Snow = new SnowGenerator(ScreenRes);
        }

        private void Overlay_Paint(object sender, PaintEventArgs e)
        {
            Graphics GDI = this.CreateGraphics();
            GDI.SmoothingMode = SmoothingMode.AntiAlias;
            GDI.InterpolationMode = InterpolationMode.Bicubic;
            GDI.PixelOffsetMode = PixelOffsetMode.HighQuality;

            /*
            // For debugging window size
            Pen Pen = new Pen(Color.Red);
            Pen.Width = 2;
            Point VerticalP1 = new Point(this.Size.Width / 2, 0);
            Point VerticalP2 = new Point(this.Size.Width / 2, this.Size.Height);
            GDI.DrawLine(Pen, VerticalP1, VerticalP2);
            Point HorizontalP1 = new Point(0, this.Size.Height / 2);
            Point HorizontalP2 = new Point(this.Size.Width, this.Size.Height / 2);
            GDI.DrawLine(Pen, HorizontalP1, HorizontalP2);
            */

            Color SnowColor = Color.FromArgb(255, 192, 246, 251);
            SolidBrush ParticleBrush = new SolidBrush(SnowColor);
            foreach (Point P in Snow.Particles)
            {
                Rectangle ParticleRect = new Rectangle(P, new Size(6, 6));
                GDI.FillEllipse(ParticleBrush, ParticleRect);
            }

            Snow.UpdateParticles();
            ParticleBrush.Dispose();
        }

        private void RedrawTimer_Tick(object sender, EventArgs e)
        {
            this.Invalidate();
        }

        private void SpawnerTimer_Tick(object sender, EventArgs e)
        {
            Snow.NewParticle();
        }
    }

    public class SnowGenerator
    {
        struct SnowParticle
        {
            Point Position;
            bool Stopped;
            
        }
        private Mutex ParticleMutex;
        public List<Point> Particles;
        private Rectangle Bounds;
        private Random RNG;
        private readonly int Speed = 2;

        public SnowGenerator(Rectangle Bounds)
        {
            this.Bounds = Bounds;
            RNG = new Random();
            Particles = new List<Point>();
            ParticleMutex = new Mutex();
        }

        public void NewParticle()
        {
            ParticleMutex.WaitOne();
            Point Position = new Point(RNG.Next(Bounds.Left, Bounds.Right), 0);
            Particles.Add(Position);
            ParticleMutex.ReleaseMutex();
        }

        public void UpdateParticles()
        {
            ParticleMutex.WaitOne();
            for (int i = 0; i < Particles.Count;)
            {
                Point P = Particles[i];

                P.X += Speed;
                if (P.X > Bounds.Right)
                    P.X = Bounds.Left;
                if (P.Y < Bounds.Bottom)
                    P.Y += Speed;
                else
                {
                    Particles.RemoveAt(i);
                    continue;
                }

                Particles[i] = P;
                i++;
            }
            ParticleMutex.ReleaseMutex();
        }
    }
}
